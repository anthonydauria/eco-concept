import type { NextPage } from 'next'
import Head from 'next/head';
import {Header, Footer} from '../../components';
import {useState} from "react";
import { useForm, Controller } from "react-hook-form";
import IconProduct from '../../public/icon.svg';
import {Button, Grid, TextField, Typography} from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

const Login: NextPage = () => {
    const validRegister = Yup.object({
        email: Yup.string()
            .required("Un email est requis")
            .email("L'email n'est pas au bon format"),
        password: Yup.string()
            .required("Un mot de passe est requis")
    });
    
    const {
        handleSubmit,
        formState: { errors },
        control,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(validRegister),
        mode: "onTouched",
    });

    const goToHome = () => {
        location.href = "/";
    }

    const goToRegister = () => {
        location.href = "/register";
    }

    return (
        <>
            <Head>
                <link rel={"shortcut icon"} href={IconProduct.src} />
                <title>Eco-service - Connexion</title>
            </Head>
            <Header/>

            <div>
                <div className={"d-inline-flex ma-1"}>
                    <Typography className={"cursor-pointer"} onClick={goToHome} variant={"body2"}>
                        Accueil
                    </Typography>
                    <Typography variant={"body2"} sx={{
                        marginLeft: "1em",
                        color: "#AAA"
                    }}>
                        Connectez-vous à votre compte
                    </Typography>
                </div>

                <Grid container className={"ma-auto w-35 pa-2 vh-63"} spacing={2}>
                    <form className={"w-100 h-60 ma-auto"}>
                        <h3>CONNECTEZ-VOUS A VOTRE COMPTE</h3>
                        <Grid item xs={"12"} className={"mb-1"}>
                            <Controller control={control} name={'email'} render={({ field }) => (
                                <TextField {...field} helperText={errors['email'] && errors['email'].message} error={errors['email'] ? true : false} placeholder={"Email"} label={'Email'} variant={"outlined"} type={"email"} className={"w-100"} size={'small'} name={"email"} />
                            )} />
                        </Grid>

                        <Grid item xs={"12"}>
                            <Controller control={control} name={'password'} render={({ field }) => (
                                <TextField {...field} helperText={errors['password'] && errors['password'].message} error={errors['password'] ? true : false} placeholder={"Password"} label={'Password'} variant={"outlined"} type={"password"} className={"w-100"} size={'small'} name={"password"} />
                            )} />
                        </Grid>

                        <Grid container spacing={2} className={"mt-2"}>
                            <Grid item xs={8} className={"text-end"}>
                                <Typography variant={'body1'} className={"cursor-pointer"}>
                                    Oublié votre mot de passe ?
                                </Typography>

                                <Typography onClick={goToRegister} variant={'body1'} className={"cursor-pointer"}>
                                    Créer un compte
                                </Typography>
                            </Grid>

                            <Grid item xs={4}>
                                <Button sx={{
                                    background: "#000",
                                    color: "#FFF",
                                    borderRadius: 0,
                                    width: '100%',
                                    height: '100%'
                                }}>S’IDENTIFIER</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </div>

            <Footer/>
        </>
    )
}

export default Login;
