import type { NextPage } from 'next'
import Head from 'next/head';
import {Header, Footer} from '../components';
import {Button, Card, CardActions, CardContent, CardMedia, Chip, Grid, Typography} from "@mui/material";
import styles from '../styles/Home.module.css';
import {useState} from "react";
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import IconProduct from '../public/icon.svg';

const Home: NextPage = () => {
    const [products, setProducts] = useState([
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        },
        {
            img: 'https://img.lignes-formations.com/wp-content/uploads/sites/45/2019/05/formation-reportage-photo-niveau-1.jpg',
            title: 'Creme thérapeutique',
            price: '15'
        }
    ]);
    return (
        <>
            <Head>
                <link rel={"shortcut icon"} href={IconProduct.src} />
                <title>Eco-service - Accueil Particulier</title>
            </Head>
            <Header/>
            <div className={"applicationPage"}>
                <div className={`${styles.bandeau} mt-0`}>
                    <Typography variant={'h6'}>Meilleurs ventes</Typography>
                </div>

                <Grid container spacing={2} className={styles.cardProduct}>
                    {
                        products.map((v, k) => {
                            return (
                                <Grid xs={12} md={4} item key={k}>
                                    <Card sx={{
                                        maxWidth: "80%"
                                    }}>
                                        <CardMedia component={"img"} height={140} image={v.img} alt={v.title} />
                                        <CardContent>
                                            <Typography variant={"body1"}>{v.title}</Typography>
                                            <Typography variant={"body1"}>{v.price} €</Typography>
                                        </CardContent>

                                        <CardActions>
                                            <Button size={"small"}>
                                                <AddShoppingCartIcon />
                                                Ajouter au panier
                                            </Button>

                                            <Button size={"small"}>
                                                Détail
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            )
                        })
                    }
                </Grid>

                <div className={styles.bandeau}>
                    <Typography variant={'h6'}>Nouveautés</Typography>
                </div>

                <Grid container spacing={2} className={styles.cardProduct}>
                    {
                        products.map((v, k) => {
                            return (
                                <Grid xs={12} md={4} item key={k}>
                                    <Card sx={{
                                        maxWidth: "80%"
                                    }}>
                                        <CardMedia component={"img"} height={140} image={v.img} alt={v.title} />
                                        <CardContent>
                                            <Typography variant={"body1"}>{v.title}</Typography>
                                            <Typography variant={"body1"}>{v.price} €</Typography>
                                        </CardContent>

                                        <CardActions>
                                            <Button size={"small"}>
                                                <AddShoppingCartIcon />
                                                Ajouter au panier
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </div>
            <Footer/>
        </>
    )
}

export default Home;
