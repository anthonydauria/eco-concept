import React from "react";
import {Controller} from "react-hook-form";
import {NextPage} from "next";
import {Footer, Header} from "../../components";
import Head from "next/head";
import IconProduct from "../../public/icon.svg";
import {Button, Grid, Typography} from "@mui/material";
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import SignpostOutlinedIcon from '@mui/icons-material/SignpostOutlined';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';

const Account: NextPage = () => {
    return (
        <>
            <Head>
                <link rel={"shortcut icon"} href={IconProduct.src} />
                <title>Eco-service - Mon compte</title>
            </Head>
            <Header/>

            <div className={'bandeau mt-0'}>
                <Typography variant={'h6'}>Nouveautés</Typography>
            </div>

            <h2 className={"textCenter"}>VOTRE COMPTE</h2>
            <div className={'logoutDiv'}>
                <Grid container spacing={3} className={'justify-center textCenter mb-4'}>
                    <Grid item xs={12} sm={3}>
                        <InfoOutlinedIcon />
                        <Typography variant={'body2'}>INFORMATION</Typography>
                    </Grid>

                    <Grid item xs={12} sm={3}>
                        <SignpostOutlinedIcon />
                        <Typography variant={'body2'}>ADRESSES</Typography>
                    </Grid>

                    <Grid item xs={12} sm={3}>
                        <AssignmentOutlinedIcon />
                        <Typography variant={'body2'}>HISTORIQUE ET DÉTAILS DES COMMANDES</Typography>
                    </Grid>
                </Grid>

                <Grid container className={'justify-center'}>
                    <Grid item xs={12} sm={3}>
                        <Button sx={{
                            background: "#000",
                            color: "#FFF",
                            borderRadius: 0,
                            width: '100%',
                            height: '100%'
                        }}>DÉCONNEXION</Button>
                    </Grid>
                </Grid>
            </div>
            <Footer/>
        </>
    )
}

export default Account;