import React from "react";
import {Controller, useForm} from "react-hook-form";
import * as Yup from 'yup';
import {NextPage} from "next";
import {Footer, Header} from "../../components";
import {yupResolver} from "@hookform/resolvers/yup";
import Head from "next/head";
import IconProduct from "../../public/icon.svg";
import {Button, FormControlLabel, Grid, Radio, RadioGroup, TextField, Typography} from "@mui/material";

const Register: NextPage = () => {
    const validRegister = Yup.object({
        firstname: Yup.string()
            .required("Un prénom est requis"),
        lastname: Yup.string()
            .required("Un nom est requis"),
        email: Yup.string()
            .required("Un email est requis")
            .email("L'email n'est pas au bon format"),
        password: Yup.string()
            .required("Un mot de passe est requis"),
        password_confirm: Yup.string()
            .required("Un mot de passe est requis")
            .oneOf([Yup.ref('password'), null], 'Les mots de passe ne correspondent pas')
    });

    const {
        handleSubmit,
        formState: { errors },
        control,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(validRegister),
        mode: "onTouched",
    });

    return (
        <>
            <Head>
                <link rel={"shortcut icon"} href={IconProduct.src} />
                <title>Eco-service - Inscription</title>
            </Head>
            <Header/>

            <div className={"d-inline-flex ma-1"}>
                <Typography variant={"h6"}>
                    Créer un compte
                </Typography>
            </div>

            <form className={"w-100 ma-auto h-100 overflow-y-auto mb-1 align-center"}>
                <Grid container direction={'row'} className={"ma-auto w-70 pa-3 vh-63"} spacing={3}>
                    {/* Sexe */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Sexe</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <RadioGroup
                            row
                            aria-labelledby="demo-row-radio-buttons-group-label"
                            name="row-radio-buttons-group"
                        >
                            <FormControlLabel value="male" control={<Radio />} label="M" />
                            <FormControlLabel value="femelles" control={<Radio />} label="Mme" />
                        </RadioGroup>
                    </Grid>

                    {/* Prenom */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Prénom</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'firstname'} render={({ field }) => (
                            <TextField {...field} helperText={errors['firstname'] && errors['firstname'].message} error={!!errors['firstname']} placeholder={"Prénom"} variant={"outlined"} className={'w-100'} type={"text"} size={'small'} name={"firstname"} />
                        )} />
                    </Grid>

                    {/* Nom */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Nom</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'lastname'} render={({ field }) => (
                            <TextField {...field} helperText={errors['lastname'] && errors['lastname'].message} error={!!errors['lastname']} placeholder={"Nom"} variant={"outlined"} className={'w-100'} type={"text"} size={'small'} name={"lastname"} />
                        )} />
                    </Grid>

                    {/* Mail */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Mail</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'email'} render={({ field }) => (
                            <TextField {...field} helperText={errors['email'] && errors['email'].message} error={!!errors['email']} placeholder={"Email"} variant={"outlined"} className={'w-100'} type={"email"} size={'small'} name={"email"} />
                        )} />
                    </Grid>

                    {/* Mot de passe */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Mot de passe</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'password'} render={({ field }) => (
                            <TextField {...field} helperText={errors['password'] && errors['password'].message} error={!!errors['password']} placeholder={"Mot de passe"} variant={"outlined"} className={'w-100'} type={"password"} size={'small'} name={"password"} />
                        )} />
                    </Grid>

                    {/* Confirmation de mot de passe */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Confirmer mot de passe</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'password_confirm'} render={({ field }) => (
                            <TextField {...field} helperText={errors['password_confirm'] && errors['password_confirm'].message} error={!!errors['password_confirm']} placeholder={"Confirmation du mot de passe"} variant={"outlined"} className={'w-100'} type={"password"} size={'small'} name={"password_confirm"} />
                        )} />
                    </Grid>

                    {/* Date de naissance */}
                    <Grid item xs={12} sm={3}>
                        <Typography variant={'body2'}>Date de naissance</Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Controller control={control} name={'birthdate'} render={({ field }) => (
                            <TextField {...field} helperText={errors['birthdate'] && errors['birthdate'].message} error={!!errors['birthdate']} placeholder={"00/00/0000"} variant={"outlined"} className={'w-100'} type={"date"} size={'small'} name={"birthdate"} />
                        )} />
                    </Grid>

                    <Grid item xs={8} />
                    <Grid item xs={4}>
                        <Button sx={{
                            background: "#000",
                            color: "#FFF",
                            borderRadius: 0,
                            width: '100%',
                            height: '100%'
                        }}>S’ENREGISTRER</Button>
                    </Grid>
                </Grid>
            </form>

            <Footer/>
        </>
    )
}

export default Register;