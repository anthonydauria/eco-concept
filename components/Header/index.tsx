import React, {FunctionComponent, useEffect, useState} from "react";
import styles from '../../styles/Header.module.css';
import {Chip, Grid, Link, TextField, Typography} from "@mui/material";
import IconProduct from '../../public/icon.svg';
import PersonIcon from '@mui/icons-material/Person';
import PersonOutlinedIcon from '@mui/icons-material/PersonOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import axios from 'axios';

const Header : FunctionComponent = () => {

    const {API_URL} = process.env;

    const [isLog, setIsLog] = useState<boolean>(false);

    const goToLogin = () => {
        location.href = `${location.origin}/login`;
    }

    // const getUser = async () => {
    //     const user = await axios.get(`${API_URL}`);
    //     console.log(user);
    //     setIsLog(!!user);
    // }
    //
    // useEffect(() => {
    //     getUser();
    // }, [])

    return (
        <div className={'header'}>
            <Grid container>
                <Grid item xs={3} className={"logoCompany"}>
                    <Link href={"/"}>
                        <img src={IconProduct.src} alt={"eco-concept"} />
                    </Link>
                </Grid>

                <Grid item xs={6} className={"d-flex flex-column justify-center"}>
                    <div className={"p-relative w-70 ma-auto"}>
                        <Chip className={"p-absolute chipCateg"} label={"Catégories"} />
                        <TextField className={"inputHeader"} size={'small'} placeholder={'Rechercher'} />
                    </div>


                    <hr className={"separatorHeader"} />

                    <div className={styles.link}>
                        <Link className={"linkHeader"} href={""}>
                            <Typography variant={'body1'}>HYGIÈNE</Typography>
                        </Link>

                        <Link className={"linkHeader"} href={""}>
                            <Typography variant={'body1'}>OUTILLAGE</Typography>
                        </Link>

                        <Link className={"linkHeader"} href={""}>
                            <Typography variant={'body1'}>JARDIN</Typography>
                        </Link>

                        <Link className={"linkHeader"} href={""}>
                            <Typography variant={'body1'}>RECETTE</Typography>
                        </Link>
                    </div>
                </Grid>

                <Grid item xs={3} className={"d-inline-flex justify-center align-center ma-auto"}>
                    {
                        isLog && (
                            <PersonIcon className={"iconAction"} />
                        ) || (
                            <PersonOutlinedIcon onClick={goToLogin} className={"iconAction"} />
                        )
                    }

                    <Typography variant={"body1"}>
                        <ShoppingCartOutlinedIcon className={"iconAction"} />
                    </Typography>
                    Panier
                </Grid>
            </Grid>
        </div>
    )
}

export default Header;