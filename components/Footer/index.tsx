import React, {FunctionComponent} from "react";
import {Grid, Typography} from "@mui/material";
import MailOutlineIcon from '@mui/icons-material/MailOutline';

const Footer : FunctionComponent = () => {
    return (
        <div className={'footer textCenter'}>
            <Grid container>
                <Grid item xs={4} className={"d-inline-flex justify-center align-center"}>
                    <Typography variant={'body1'}>
                        Mentions légales I Politique de confidentialité
                    </Typography>
                </Grid>

                <Grid item xs={4} className={"d-inline-flex justify-center align-center"}>
                    <MailOutlineIcon />
                    <Typography className={"ml-2"} variant={'body1'}>
                        Aide et contact
                    </Typography>
                </Grid>

                <Grid item xs={4} className={"d-inline-flex justify-center align-center"}>
                    <Typography variant={'body1'}>
                        Paiements sécurisés
                    </Typography>
                    <div className={"imgFooter"} />
                </Grid>
            </Grid>
        </div>
    )
}

export default Footer;